console.log("hello there, General Kenobi");

const metaBtn = document.querySelector(".meta-btn");
const menu = document.querySelector(".header-navbar");

metaBtn.addEventListener("click", (e) => {
    metaBtn.classList.toggle("active");
    menu.classList.toggle("active");
    createOverlay();
});

function createOverlay() {
    if (metaBtn.classList.contains("active")) {
        const overlay = document.createElement("div");
        overlay.classList.add("overlay");
        document.body.append(overlay);
        overlay.addEventListener("click", (e) => {
            metaBtn.classList.remove("active");
            menu.classList.remove("active");
            overlay.remove();
        });
    }
}